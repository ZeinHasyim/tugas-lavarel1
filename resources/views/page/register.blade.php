<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Daftar</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf 
        <label> First Name:</label> <br><br />
        <input type="text" name="namadepan"> <br><br />
        <label> Last Name:</label> <br><br />
        <input type="text" name="namabelakang" /> <br><br />
        <label> Gender</label> <br /><br />
        <input type="radio" name="gender" checked /> Male
        <input type="radio" name="gender">Female
        <input type="radio" name="gender">Other <br /><br />
        <label>Nationality</label><br /><br />
        <select name="Nationality">
            <option value="Indonesia"> Indonesian</option>
            <option value="singapore"> Singaporean</option>
            <option value="Malaysia"> Malaysian</option>
        </select> <br /><br />
        <label>Language</label> <br /><br />
        <input type="checkbox" />Bahasa Indonesia
        <input type="checkbox" />English
        <input type="checkbox" />Other <br /><br />
        <label>Bio</label><br /><br />
        <textarea name="Bio" cols="30" rows="10"></textarea> <br />
        <input type="submit" value="Sign Up" />

    </form>

</body>
</html>
